function renderTablePatientsData(data, tableBody) {
    for (let patient of data.patients) {
        tableRow = `
        <tr>
            <td>${patient.id}</td>
            <td>${patient.first_name}</td>
            <td>${patient.last_name}</td>
            <td>${patient.middle_name}</td>
            <td>${patient.gender}</td>
            <td>${patient.birthday}</td>
            <td>
                <div class="d-flex">
                    <a type="button" class="action-edit btn btn-danger" href="patients.html?patientId=${patient.id}">
                        <i style="pointer-events: none" class="bi bi-pencil"></i>
                    </a>
                    <button type="button" class="action-remove btn btn-primary mx-2" data-id="${patient.id}">
                        <i style="pointer-events: none" class="bi bi-trash"></i>
                    </button>
                </div>
            </td>
        </tr>
        `;
        tableBody.append(tableRow);
    }
}

$(function() {

    const tableBody = $('#patients');
    const data = getData();

    if (data.patients.length > 0) {
        renderTablePatientsData(data, tableBody);
    } else {
        tableEmptyRow = `
            <tr>
                <td colspan="5">No patients found.</td>
            </tr>
        `;
        tableBody.append(tableEmptyRow);
    }

    $('.action-remove').on('click', function(event) {
        let itemId = Number(event.target.dataset.id);
        data.patients = data.patients.filter((p) => p.id !== itemId);
        saveDataToLocalStorage(data);
        location.reload();
    });

});