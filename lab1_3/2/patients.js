$(function() {
    let params = new URLSearchParams(window.location.search);
    let patientId = 0;

    if (params.has('patientId')) {
        patientId = Number(params.get('patientId'));
        const data = getData();
        const patient = data.patients.find((p) => p.id === patientId);
        $('#first_name').val(patient.first_name);
        $('#last_name').val(patient.last_name);
        $('#middle_name').val(patient.middle_name);
        $('#gender').val(patient.gender);
        $('#birthday').val(patient.birthday);
    }

    $('#action-save').on('click', function() {
        const data = getData();
        if (patientId === 0) {
            data.patients.push({
                'id': data.patients[data.patients.length - 1].id + 1,
                'first_name': $('#first_name').val(),
                'last_name': $('#last_name').val(),
                'middle_name': $('#middle_name').val(),
                'gender': $('#gender').val(),
                'birthday': $('#birthday').val(),
            });
        } else {
            const patient = data.patients.find((p) => p.id === patientId);
            patient.first_name = $('#first_name').val();
            patient.last_name = $('#last_name').val();
            patient.middle_name = $('#middle_name').val();
            patient.gender = $('#gender').val();
            patient.birthday = $('#birthday').val();
        }
        saveDataToLocalStorage(data);
        $(location).attr('href', 'index.html');
    });
});