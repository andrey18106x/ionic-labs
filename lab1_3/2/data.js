function loadDummyData() {
    return {
        patients: [
            {
                'id': 1,
                'first_name': 'Andrey',
                'last_name': 'Borysenko',
                'middle_name': 'Alexandrovich',
                'gender': 'male',
                'birthday': '2000-11-21',
                'diseases': [],
            },
            {
                'id': 2,
                'first_name': 'Ivan',
                'last_name': 'Ivanov',
                'middle_name': 'Ivanovich',
                'gender': 'male',
                'birthday': '2001-03-07',
                'diseases': [],
            },
        ],
        diseases: [
            {
                'id': 1,
                'name': 'Disease1',
                'mortality': 10,
                'step': 1,
            },
            {
                'id': 2,
                'name': 'Disease1_2',
                'mortality': 20,
                'step': 2,
            },
            {
                'id': 3,
                'name': 'Disease2',
                'mortality': 5,
                'step': 1,
            },
        ]
    };
}

function loadDataFromLocalStorage() {
    let patients = JSON.parse(localStorage.getItem('hospital_patients'));
    let diseases = JSON.parse(localStorage.getItem('hospital_diseases'));
    return { patients: patients, diseases: diseases };
}

function saveDataToLocalStorage(data) {
    localStorage.setItem('hospital_patients', JSON.stringify(data.patients));
    localStorage.setItem('hospital_diseases', JSON.stringify(data.diseases));
}

function getData() {
    let data = loadDataFromLocalStorage();
    if (data.patients === null && data.diseases === null) {
        data = loadDummyData();
        saveDataToLocalStorage(data);
    }
    return data;
}