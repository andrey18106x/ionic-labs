import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  data: any = null;

  constructor() {
    this.loadData();
  }

  loadDummyData() {
    return {
      patients: [
        {
          'id': 1,
          'first_name': 'Andrey',
          'last_name': 'Borysenko',
          'middle_name': 'Alexandrovich',
          'gender': 'male',
          'birthday': '2000-11-21',
          'diseases': [],
        },
        {
          'id': 2,
          'first_name': 'Ivan',
          'last_name': 'Ivanov',
          'middle_name': 'Ivanovich',
          'gender': 'male',
          'birthday': '2001-03-07',
          'diseases': [],
        },
      ],
      diseases: [
        {
          'id': 1,
          'name': 'Disease1',
          'mortality': 10,
          'step': 1,
        },
        {
          'id': 2,
          'name': 'Disease1_2',
          'mortality': 20,
          'step': 2,
        },
        {
          'id': 3,
          'name': 'Disease2',
          'mortality': 5,
          'step': 1,
        },
      ]
    };
  }

  loadDataFromLocalStorage() {
    let patients = JSON.parse(localStorage.getItem('hospital_patients'));
    let diseases = JSON.parse(localStorage.getItem('hospital_diseases'));
    return { patients: patients, diseases: diseases };
  }

  saveDataToLocalStorage(data: any) {
      localStorage.setItem('hospital_patients', JSON.stringify(data?.patients));
      localStorage.setItem('hospital_diseases', JSON.stringify(data?.diseases));
  }

  loadData(): void {
    let data = this.loadDataFromLocalStorage();
    if (data.patients === null && data.diseases === null) {
      data = this.loadDummyData();
      this.saveDataToLocalStorage(data);
    }
    this.data = data;
  }

  get(itemId: number, folderId: string): any {
    const itemIndex = this.data[folderId.toLowerCase()].findIndex((i: any) => i.id === itemId);
    if (itemIndex !== -1) {
      return this.data[folderId.toLowerCase()][itemIndex];
    }
    return null;
  }

  add(item: any, folderId: string): void {
    const itemIndex = this.data[folderId.toLowerCase()].findIndex((i: any) => i.id === item.id);
    if (itemIndex === -1) {
      this.data[folderId.toLowerCase()].push(item);
      this.saveDataToLocalStorage(this.data);
    }
  }

  update(item: any, folderId: string): void {
    const itemIndex = this.data[folderId.toLowerCase()].findIndex((i: any) => i.id === item.id);
    this.data[folderId.toLowerCase()][itemIndex] = item;
    this.saveDataToLocalStorage(this.data);
  }

  delete(item: any, folderId: string): void {
    this.data[folderId.toLowerCase()] = this.data[folderId.toLowerCase()].filter((i: any) => i.id !== item.id);
    this.saveDataToLocalStorage(this.data);
  }
}
