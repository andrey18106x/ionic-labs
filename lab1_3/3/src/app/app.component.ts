import { Component } from '@angular/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  public appPages = [
    { title: 'Patients', url: '/folder/Patients', icon: 'list' },
    { title: 'Diseases', url: '/folder/Diseases', icon: 'list' },
  ];
  public labels = ['All', 'Danger', 'Medium', 'Light'];
  constructor() {}
}
