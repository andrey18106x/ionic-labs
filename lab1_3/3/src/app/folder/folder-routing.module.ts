import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddItemComponent } from './components/add-item/add-item.component';
import { EditItemComponent } from './components/edit-item/edit-item.component';
import { ItemComponent } from './components/item/item.component';
import { ItemsComponent } from './components/items/items.component';

const routes: Routes = [
  {
    path: ':folderId',
    component: ItemsComponent,
  },
  {
    path: ':folderId/add',
    component: AddItemComponent,
  },
  {
    path: ':folderId/:id',
    component: ItemComponent,
  },
  {
    path: ':folderId/:id/edit',
    component: EditItemComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FolderPageRoutingModule {}
