import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss'],
})
export class EditItemComponent implements OnInit {

  public folder: string;
  item: any;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('folderId');
    this.dataService.loadData();
    this.item = this.dataService.get(Number(this.activatedRoute.snapshot.paramMap.get('id')), this.folder);
  }

  get data(): any {
    return this.dataService.data[this.folder.toLowerCase()];
  }

  update(): void {
    this.dataService.update(this.item, this.folder);
    this.router.navigate(['folder', this.folder]);
  }

}
