import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
})
export class AddItemComponent implements OnInit {

  public folder: string;
  public item: any;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('folderId');
    this.initItem();
    this.dataService.loadData();
  }

  initItem(): void {
    if (this.folder === 'Patients') {
      this.item = {
        id: '',
        last_name: '',
        first_name: '',
        middle_name: '',
        gender: '',
        birthday: '',
      };
    } else {
      this.item = {
        id: '',
        name: '',
        mortality: '',
        step: '',
      };
    }
  }

  add(): void {
    this.item.id = this.dataService.data[this.folder.toLowerCase()][this.dataService.data[this.folder.toLowerCase()].length - 1].id + 1;
    this.dataService.add(this.item, this.folder);
    this.router.navigate(['folder', this.folder]);
  }
}
