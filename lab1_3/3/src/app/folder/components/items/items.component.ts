import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
})
export class ItemsComponent implements OnInit {

  public folder: string;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService,
              private router: Router) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('folderId');
    this.dataService.loadData();
  }

  view(item: any): void {
    this.router.navigate(['folder', this.folder, item.id]);
  }

  add(): void {
    this.router.navigate(['folder', this.folder, 'add']);
  }

  edit(item: any) {
    this.router.navigate(['folder', this.folder, item.id, 'edit']);
  }

  delete(item: any) {
    this.dataService.delete(item, this.folder);
  }

  get items(): any {
    return this.dataService.data[this.folder.toLowerCase()];
  }

}
