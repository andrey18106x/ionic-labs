import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {

  public folder: string;
  public item: any;

  constructor(private activatedRoute: ActivatedRoute, private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    this.folder = this.activatedRoute.snapshot.paramMap.get('folderId');
    this.dataService.loadData();
    this.item = this.dataService.get(Number(this.activatedRoute.snapshot.paramMap.get('id')), this.folder);
  }

  edit() {
    this.router.navigate(['folder', this.folder, this.item.id, 'edit']);
  }

  delete() {
    this.dataService.delete(this.item, this.folder);
    this.router.navigate(['folder', this.folder]);
  }

}
